FROM ruby:2.4.1-alpine
MAINTAINER Tiago Gondim <tiago.gondim@skyhub.com.br>

RUN apk add --update \
  build-base \
  nodejs \
  tzdata \
  && rm -rf /var/cache/apk/*

RUN gem install bundler

# First copy the bundle files and install gems to aid caching of this layer
WORKDIR /myapp
COPY Gemfile* /myapp/
RUN bundle install

WORKDIR /myapp
COPY . /myapp

CMD /bin/sh -c "rm -f /myapp/tmp/pids/server.pid && ./bin/rails server -p 80 -b 0.0.0.0"
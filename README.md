# CadastroProdutos-Desafio

Cadastro de Produtos implementado como treinamento para familiarização com o stack de desenvolvimento.

##Desafio #1

A aplicação foi criada utilizando o framework rails através do scaffold. A persistência é feita num banco não relacional MongoDB. 

##Desafio #2

O Redis foi integrado ao cadastro para implementar o cache para a visualizaçao dos proutos.

##Desafio #3

Integramos o ElastiSearch com o cadastro para permitir uma busca de produtos mais eficiente. 
Usamos também ElasticSearch + Logstash + Kibana para implementar a visualização e análise de logs da aplicação.

##Desafio #4

Foram adicionadas algumas validações no produto como:
	- SKU só pode ter alfa-numéricos e hífen;
	- Nome é obrigatório;
	- Preço sempre maior que zero;
	- Produto deve ter mais um campo “Código de barras” (de 8 a 13 dígitos).
Estas validações foram implementadas usando a metodologia TDD com o auxílio do RSpec.

##Desafio #5

Implementamos a solicitação de gerar, em background integrado com Sidekiq, relatório do cadastro de produtos no formato CSV.
O arquivo gerado tem o nome no formato "RelatorioProdutos_DD-MM-YYYY.csv" e armazenado na pasta ./reports

# Atenção! 

Para executar este projeto, recomendam-se os seguintes requisistos:

- Ruby (2.2.9 or later);

- Rails (4.2.9 or later);

- Redis (4.0.1 or later, on localhost:6379);

- MongoDB (3.6.4 or later, on localhost:27017) sem autenticação de segurança;

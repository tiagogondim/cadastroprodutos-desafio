require 'rails_helper'

RSpec.describe Produto, :type => :model do
  subject { described_class.new }

  #testa sku
  it "sku válido com caracteres alfanuméricos e hifen" do
    subject.sku = 'Anything-You-Want-01'
    subject.nome = 'Nome Completo'
    subject.preco = 10
    subject.ean = '1234567890123'
    expect(subject).to be_valid
  end

  it "sku inválido com caracteres especiais" do
    subject.sku = 'Anything$You#Want-01'
    subject.nome = 'Nome Completo'
    subject.preco = 10
    subject.ean = '1234567890123'
    expect(subject).to_not be_valid
  end

  it "sku inválido se estiver vazio" do
    subject.sku = ''
    subject.nome = 'Nome Completo'
    subject.preco = 10
    subject.ean = '1234567890123'
    expect(subject).to_not be_valid
  end

  it "sku inválido se estiver nulo" do
    subject.sku = ''
    subject.nome = 'Nome Completo'
    subject.preco = 10
    subject.ean = '1234567890123'
    expect(subject).to_not be_valid
  end

  #testa nome
  it "nome válido com caracteres especiais" do
    subject.sku = 'Anything-You-Want-01'
    subject.nome = 'Nome Completo Mc\'donalds'
    subject.preco = 10
    subject.ean = '1234567890123'
    expect(subject).to be_valid
  end

  it "nome inválido se estiver nulo" do
    subject.sku = 'Anything-You-Want-01'
    subject.nome = nil
    subject.preco = 10
    subject.ean = '1234567890123'
    expect(subject).to_not be_valid
  end

  it "nome inválido se estiver vazio" do
    subject.sku = 'Anything-You-Want-01'
    subject.nome = ''
    subject.preco = 10
    subject.ean = '1234567890123'
    expect(subject).to_not be_valid
  end

  #testa preco
  it "preco válido maior que 0" do
    subject.sku = 'Anything-You-Want-01'
    subject.nome = 'Nome Completo Mc\'donalds'
    subject.preco = 10
    subject.ean = '1234567890123'
    expect(subject).to be_valid
  end

  it "preco inválido nulo" do
    subject.sku = 'Anything-You-Want-01'
    subject.nome = 'Nome Completo Mc\'donalds'
    subject.preco = nil
    subject.ean = '1234567890123'
    expect(subject).to_not be_valid
  end

  it "preco inválido igual a 0" do
    subject.sku = 'Anything-You-Want-01'
    subject.nome = 'Nome Completo Mc\'donalds'
    subject.preco = 0
    subject.ean = '1234567890123'
    expect(subject).to_not be_valid
  end

  it "preco inválido menor que 0" do
    subject.sku = 'Anything-You-Want-01'
    subject.nome = 'Nome Completo Mc\'donalds'
    subject.preco = -10
    subject.ean = '1234567890123'
    expect(subject).to_not be_valid
  end

  #testa ean
  it "ean válido com mínimo de 8 caracteres e máximo de 13" do
    subject.sku = 'Anything-You-Want-01'
    subject.nome = 'Nome Completo Mc\'donalds'
    subject.preco = 10
    subject.ean = '1234567890123'
    expect(subject).to be_valid
  end

  it "ean inválido com menos de 8 caracteres" do
    subject.sku = 'Anything-You-Want-01'
    subject.nome = 'Nome Completo Mc\'donalds'
    subject.preco = 10
    subject.ean = '1234567'
    expect(subject).to_not be_valid
  end

  it "ean inválido com mais de 13 caracteres" do
    subject.sku = 'Anything-You-Want-01'
    subject.nome = 'Nome Completo Mc\'donalds'
    subject.preco = 10
    subject.ean = '12345678901234'
    expect(subject).to_not be_valid
  end

end
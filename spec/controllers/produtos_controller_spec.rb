require 'rails_helper'

RSpec.describe ProdutosController, type: :controller do
    subject { described_class.new }

  #testa report_request
  it "inserindo um produto, é gerado o arquivo csv com apenas este produto" do
    Produto.collection.drop
    produto = Produto.new
    produto.sku = '1234-ABCD'
    produto.nome = 'Nome Teste'
    produto.descricao = 'Descricao teste.'
    produto.quantidade = 10
    produto.preco = 25.12
    produto.ean = '3210123456789'
    produto.save

    file_like_it_is = subject.send :build_report

    file_like_must_be = "sku,nome,descricao,quantidade,preco,ean\r\n" \
                                    + produto.sku + ',' \
                                    + produto.nome + ',' \
                                    + produto.descricao + ',' \
                                    + produto.quantidade.to_s + ',' \
                                    + produto.preco.to_s + ',' \
                                    + (produto.ean.nil? ? ' ' : produto.ean) + "\r\n"
    expect(file_like_must_be).to eq(file_like_it_is)
  end

  it "ao gerar relatorio sem produtos cadastrados, imprime apenas a linha com nomes das colunas" do
    Produto.collection.drop

    file_like_it_is = subject.send :build_report

    file_like_must_be = "sku,nome,descricao,quantidade,preco,ean\r\n"
    expect(file_like_must_be).to eq(file_like_it_is)
  end
end

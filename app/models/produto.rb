# require "csv"

class Produto
  include Mongoid::Document
  include Mongoid::Timestamps

  validates :sku, format: {with: /^[A-Za-z0-9-]+$/, message: "Não pode ser vazio. Caracteres podem ser apenas [a-z 0-9 -]" , :multiline => true }
  validates_presence_of :nome
  validates :preco, numericality: { greater_than: 0 }
  validates :ean, length: {:allow_blank => true, :in => 8..13 }

  field :sku, type: String
  field :nome, type: String
  field :descricao, type: String
  field :quantidade, type: Integer
  field :preco, type: Float
  field :ean, type: String

  searchkick callbacks: :async

end

require 'date'

class ProdutosController < ApplicationController
  skip_before_action :verify_authenticity_token, :only => [:create, :update, :destroy]
  before_action :set_produto, only: [:show, :edit, :update, :destroy]
  after_action :update_produto_on_redis, only: [:edit, :update]
  after_action :destroy_produto_on_redis, only: [:destroy]

  # GET /produtos_url
  # GET /produtos.json
  def index
     if params[:keywords].present?
        Produto.reindex
        @produtos = Produto.search params[:keywords], fields: [:sku, :nome, :descricao]
      else
        @produtos = Produto.all
      end
      #report_notice = ' '
  end

  # GET /produtos/1
  # GET /produtos/1.json
  def show
  end

  # GET /produtos/new
  def new
    @produto = Produto.new
  end

  # GET /produtos/1/edit
  def edit
  end

  # POST /produtos
  # POST /produtos.json
  def create
    @produto = Produto.new(produto_params)
    respond_to do |format|
      if @produto.save
        format.html { redirect_to @produto, notice: 'Produto foi criado com sucesso.' }
        format.json { render :show, status: :created, location: @produto }
      else
        format.html { render :new }
        format.json { render json: @produto.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /produtos/1
  # PATCH/PUT /produtos/1.json
  def update
    respond_to do |format|
      if @produto.update(produto_params)
        format.html { redirect_to @produto, notice: 'Produto foi atualizado com sucesso.' }
        format.json { render :show, status: :ok, location: @produto }
      else
        format.html { render :edit }
        format.json { render json: @produto.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /produtos/1
  # DELETE /produtos/1.json
  def destroy
    @produto.destroy
    respond_to do |format|
      format.html { redirect_to produtos_url, notice: 'Produto foi removido com sucesso.' }
      format.json { head :no_content }
    end
  end

  # GET /produtos/report/request
  def report_request
    if params[:send_to_email].present?
      send_to = params[:send_to_email]

      report_content = build_report

      d = DateTime.now
      report_filename = 'RelatorioCadastroProdutos_' + d.strftime("%d-%m-%Y") + '.csv'
      send_report_mail_request(send_to, 'Relatório de Cadastro de Produtos', report_filename, report_content)

      $logger.info('Report generated and sent to send email queue through MailSender API')

      respond_to do |format|
          format.html { redirect_to produtos_url, notice: 'O relatório foi solicitado e será enviado para seu email em breve.' }
          format.json { render :show, status: :ok, location: @produto }
      end
    else
      redirect_to produtos_url
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_produto
      if defined? params[:id]
        @produto = get_produto_from_redis(params[:id])
      end
    end

    def update_produto_on_redis
      $redis.set(@produto._id, Marshal.dump(@produto))
    end

    def destroy_produto_on_redis
      $redis.del(params[:id])
    end

    def get_produto_from_redis(produto_id)
      if  $redis.exists(produto_id)
        produto = Marshal.load($redis.get(produto_id))
        $logger.info('Getting ' + produto_id + ' from Redis cache')
      else
        produto = Produto.find(produto_id)
        $redis.set(produto_id, Marshal.dump(produto))
        $logger.info('Adding ' + produto_id + ' to Redis cache')
      end

      return produto
    end

    def build_report
      produtos = Produto.all
      report_content = "sku,nome,descricao,quantidade,preco,ean\r\n"

      produtos.each do |produto|
        report_content += produto.sku + ',' \
                                      + produto.nome + ',' \
                                      + produto.descricao + ',' \
                                      + produto.quantidade.to_s + ',' \
                                      + produto.preco.to_s + ',' \
                                      + (produto.ean.nil? ? ' ' : produto.ean) + "\r\n"
      end

      return report_content
    end

    def send_report_mail_request(to, subject, report_filename, report_content)
      require 'net/http'
      require 'json'
      begin
        send_report_mail_endpoint = Rails.configuration.send_report_mail_request_endpoint
        uri = URI(send_report_mail_endpoint)
        http = Net::HTTP.new(uri.host, uri.port)
        req = Net::HTTP::Post.new(uri.path, {'Content-Type' =>'application/json'})
        req.body = {"to" => to, "subject" => subject, "report_filename" => report_filename, "report_content" => report_content}.to_json
        res = http.request(req)
        puts "response #{res.body}"
        puts JSON.parse(res.body)
      rescue => e
        $logger.error("failed #{e}")
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def produto_params
      params.require(:produto).permit(:sku, :nome, :descricao, :quantidade, :preco, :ean)
    end

end

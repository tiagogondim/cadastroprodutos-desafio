require 'redis'
require 'redis_config'

begin
  RedisConfig.config = YAML.load_file("config/redis.yml")[Rails.env].symbolize_keys
  $redis = Redis.new(:host => RedisConfig.config[:host], :port =>  RedisConfig.config[:port])
rescue Exception => e
  puts e
end
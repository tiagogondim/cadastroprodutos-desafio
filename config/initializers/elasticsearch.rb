require 'elasticsearch'

url = Rails.configuration.elasticsearch_url
Searchkick.client = Elasticsearch::Client.new(hosts: url, retry_on_failure: true, transport_options: {request: {timeout: 250}})